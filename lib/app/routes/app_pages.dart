import 'package:flutter/material.dart';
import 'package:get/get.dart';
// --- Pages
import 'package:mobile/app/modules/home/views/home_screen.dart';
import 'package:mobile/app/modules/session/views/splash_screen.dart';

part './app_routes.dart';

class AppPages {
  static final pages = [
    // ------------- Session
    // ++
    // ++
    // ++
    GetPage(
      name: Routes.INITIAL,
      page: () => SplashScreen(),
    ),
    // ++
    // ++
    // ++
    // --------------  home
    // ++
    // ++
    // ++
    GetPage(
      name: Routes.HOME,
      transition: Transition.noTransition,
      curve: Curves.easeInOut,
      page: () => HomeScreen(),
    ),
  ];
}
