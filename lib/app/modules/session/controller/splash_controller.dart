import 'dart:async';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:mobile/app/routes/app_pages.dart';
import 'package:mobile/app/utils/helpers/session.helper.dart';

class SplashController extends GetxController {
  SessionManager prefs = SessionManager();

  @override
  void onReady() {
    // TODO: implement onReady()
    // prefs.logout();
    // print(prefs.isUserLogged());
    if (prefs.isUserLogged()) {
      ///Go to Shop home Screen
      print("User is already logged");
      // Fluttertoast.showToast(msg: "Go to home page screen");
      // Timer(Duration(seconds: 3), onPageHome);
      onPageHome();
    } else {
      print("No User !!!");
      // Timer(Duration(seconds: 3), onPageStart);
      if (prefs.isTmpUserLogged()) {
        Timer(Duration(seconds: 2), onAccountNotValid);
      } else {
        onPageStart();
      }
    }
    super.onReady();
  }

  void onPageStart() {
    Get.offAndToNamed(Routes.WELCOME);
  }

  void onAccountNotValid() {
    Get.offAndToNamed(Routes.OTP);
  }

  void onPageHome() {
    Get.offAndToNamed(Routes.HOME);
  }
}
